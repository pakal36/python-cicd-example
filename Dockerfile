# Slim is based on debian, which has troubles resolving DNS alias that point to
# *.xxx like *.prod-blue.he.k8s.emea.adsint.biz. For this reason It is best to
# use alpine.
# FROM python:2-slim

FROM python:2.7-alpine

COPY dist/*.whl /tmp
RUN python -m pip install /tmp/*.whl

EXPOSE 8080

ENTRYPOINT [ "myapp"]

