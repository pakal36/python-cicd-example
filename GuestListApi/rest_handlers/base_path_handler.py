import tornado.web
import json


class BasePathHandler(tornado.web.RequestHandler):

    def get(self):
        self.write({'message': 'Hello. This is the Guest List management API '})
