import json


class GuestList:

    def __init__(self):
        self.__guests_list = []

    def add_guest(self, guest_name):
        self.__guests_list.append(guest_name)

    def remove_guest(self, guest_name):
        self.__guests_list.remove(guest_name)

    def get_guest_list(self):
        return self.__guests_list

    def json_list(self):
        return json.dumps(self.__guests_list)
