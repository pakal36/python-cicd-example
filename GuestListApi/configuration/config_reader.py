from environs import Env
from marshmallow.validate import Range


class ConfigReader():

    def __init__(self):

        env = Env()
        self.lister_port = env.int(
            "LISTEN_PORT",
            default=8080,
            validate=Range(
                min=0, max=65535,
                error="LISTEN_PORT should be an int between 0 and 65535"
            )
        )

        # There are several validators already implented. List here:
        # https://marshmallow.readthedocs.io/en/3.0/_modules/marshmallow/validate.html

        # Non mandatory Fields are achieved by setting a default value
        # var2 = env.int("NOT_MANDATORY", default=None)

        # Mandatory fields are made by not setting a refault value
        # var3 = env.int("MANDATORY")








