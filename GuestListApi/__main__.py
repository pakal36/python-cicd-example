import logging
from tornado.ioloop import IOLoop
from tornado.web import Application
from GuestListApi.base_classes.guest_list import GuestList
from GuestListApi.configuration.config_reader import ConfigReader
from GuestListApi.rest_handlers.base_path_handler import BasePathHandler
from GuestListApi.rest_handlers.guest_list_handler import \
    GuestListHandler


def make_app():
    guest_list = GuestList()
    urls = [
        ("/", BasePathHandler),
        ("/guest_list", GuestListHandler, dict(guest_list=guest_list)),
    ]

    return Application(urls)

def apply_logging_config():
    logging.basicConfig(
        format='%(asctime)s - %(levelname)s - %(message)s [%(filename)s:%(lineno)d]',
        level=logging.getLevelName("INFO")
    )

def main():
    apply_logging_config()
    config_reader = ConfigReader()
    app = make_app()
    app.listen(config_reader.lister_port)
    IOLoop.current().start()


if __name__ == "__main__":
    main()