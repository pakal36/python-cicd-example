import os
from unittest import TestCase
from environs import EnvError
from GuestListApi.configuration.config_reader import ConfigReader


class TestConfigReader(TestCase):

    def setUp(self):
        pass

    def test_unvalid_port_env_var_raises_exception(self):

        os.environ["LISTEN_PORT"] = "70000"
        with self.assertRaises(EnvError):
            config = ConfigReader()

        os.environ["LISTEN_PORT"] = "-10"
        with self.assertRaises(EnvError):
            config = ConfigReader()




