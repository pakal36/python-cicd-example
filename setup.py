from setuptools import setup, find_packages
setup(
    name="GuestListApi",
    version="0.0.1",

    # Manifest dependencies
        # Project uses reStructuredText, so ensure that the docutils get
        # installed or upgraded on the target machine
    install_requires=[
        'docutils>=0.3',
        'tornado==5.1.1',
        'environs==4.1.0'
    ],

    # version specifier for the Python version, used to specify the Requires-Python
    python_requires='~=2.7',

    # Automatically includes all packages containg a __init__.py
    packages=find_packages(),

    # Also include the Readme.md file in the deliverable
    package_data={
        # If any package contains *.txt or *.rst files, include them:
        '': ['*.md', ]
    },

    # All tests are in the tests package
    test_suite="tests",

    # The way to use this define "entry points" in your setup script that
    # indicate what function the generated script should import and run.
    # It is very similar to MVN main class spec when creating a jar.
    # Setup tools will even create a .exe wrapper for the entrpoiny when in
    # windws.
    entry_points={
        'console_scripts': [
            'myapp = GuestListApi.__main__:main',

        ]
    },


    # metadata to display on PyPI
    author="Pablo Calvo",
    author_email="pablo.calvo@externals.adidas.com",
    description="Ecom query timer exporter for prometheus",
    license="PSF",
    url="https://tools.adidas-group.com/bitbucket/projects/BAM2/repos/bam-ecom-query-exporter/browse",   # project home page, if any
    project_urls={
        "Source Code": "https://tools.adidas-group.com/bitbucket/projects/BAM2/repos/bam-ecom-query-exporter/browse",
    }

)