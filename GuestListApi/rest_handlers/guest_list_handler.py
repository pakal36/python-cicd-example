import logging
import tornado.web
import json


class GuestListHandler(tornado.web.RequestHandler):

    def initialize(self, guest_list):
        self.guest_list = guest_list

    def post(self):
        logging.info("Handling POST")
        body = tornado.escape.json_decode(self.request.body)

        self.guest_list.add_guest(body["name"])
        self.write("Added guest {0}".format(body["name"]))

    def delete(self):
        logging.info("Handling DELETE")
        name = self.get_argument('name')
        self.guest_list.remove_guest(name)
        self.write("Deleted Guest {0} succsessfully".format(name))
        self.set_status(200)

    def get(self):
        logging.info("Handling GET")
        self.write(self.guest_list.json_list())